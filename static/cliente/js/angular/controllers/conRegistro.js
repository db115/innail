var app = angular.module('appPrincipal');

app.controller('conRegistro', ['$scope', '$http', function($scope, $http){
    $scope.user = {nombre: '', email: '', telefono: '', password: ''};
    
    $scope.registrar = function(email, password){
        $http({
            method: 'POST',
            url: '/cliente/registro/',
            data: $.param({nombre: $scope.nombre, email: $scope.email, telefono: $scope.telefono, password: $scope.password})
        }).then(
            function(response){
                if (response.data === "Registrado") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: check'></span> Bienvenido", 
                        status: 'success', 
                        pos: 'top-center', 
                        timeout: 5000 
                    });
                    
                    window.location.href = '/'; 
                }else if (response.data === "Cliente ya existe") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> El correo ya está registrado", 
                        status: 'warning', 
                        pos: 'top-center', 
                        timeout: 5000 
                    });
                }else if (response.data === "Faltan datos") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> Ingrese todos los datos", 
                        status: 'warning', 
                        pos: 'top-center', 
                        timeout: 5000 
                    });
                }
            },
            function(response){
                UIkit.notification({
                    message: "<span uk-icon='icon: warning'></span> Compruebe sus datos de acceso",
                    status: 'warning',
                    pos: 'top-center',
                    timeout: 5000
                });
                $scope.user = response2.data || 'Request failed';
            }
        );
    }
}])
