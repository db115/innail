var app = angular.module('appPrincipal');

app.controller('conCatalogo', ['$scope', '$http', function($scope, $http){
    
    $scope.traerServicios = function(){
        $http.get('/servicios/').then(
            function(response){
                $scope.servicios = response.data;
                console.log($scope.repuestoslistado);
            },
            function(response){
                $scope.repuestoslistado = response.data || 'Request failed';
            }
        );
    };
    
    $scope.traerServicios();
    
}])
