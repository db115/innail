var app = angular.module('appPrincipal');

app.controller('conIngreso', ['$scope', '$http', function($scope, $http){
    $scope.user = {email: '', password: ''};
    
    $scope.ingresar = function(email, password){
        $http({
            method: 'POST',
            url: '/ingreso/',
            data: $.param({email: $scope.email, password: $scope.password})
        }).then(
            function(response){                
                if (response.data === "1" || response.data === "2") {
                    UIkit.notification({
                        message: "<span uk-icon='icon: check'></span> Bienvenido",
                        status: 'success',
                        pos: 'top-center',
                        timeout: 5000
                    });
                    
                    window.location.href = '/control/tablero/';
                }else if (response.data === "3") {
                    UIkit.notification({
                        message: "<span uk-icon='icon: check'></span> Bienvenido",
                        status: 'success',
                        pos: 'top-center',
                        timeout: 5000
                    });
                    
                    window.location.href = '/';
                }else if (response.data === "Datos incorrectos") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> "+response.data, 
                        status: 'danger', 
                        pos: 'top-center',     
                        timeout: 5000 
                    });
                }
            },
            function(response){
                UIkit.notification({
                    message: "<span uk-icon='icon: warning'></span> Compruebe sus datos de acceso",
                    status: 'warning',
                    pos: 'top-center',
                    timeout: 5000
                });
                $scope.user = response.data || 'Request failed';
            }
        );
    }
}])
