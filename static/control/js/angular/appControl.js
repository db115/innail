angular.module('appControl', []);

var my_app = angular
        .module('appControl').config(
            function($interpolateProvider, $httpProvider) {
                $interpolateProvider.startSymbol('{$');
                $interpolateProvider.endSymbol('$}');
                $httpProvider.defaults.xsrfCookieName = 'csrftoken';
                $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
                $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
                $httpProvider.defaults.withCredentials = true;
                $httpProvider.defaults.cache = true;
            }
        );
