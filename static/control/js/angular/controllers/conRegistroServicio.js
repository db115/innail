var app = angular.module('appControl');

app.controller('conRegistroServicio', ['$scope', '$http', function($scope, $http){
    $scope.user = {nombre: '', descripcion: '', precio: ''};
    
    $scope.registrar = function(){
        $http({
            method: 'POST',
            url: '/administrador/servicios/ofertar/',
            data: $.param({nombre: $scope.nombre, descripcion: $scope.descripcion, precio: $scope.precio})
        }).then(
            function(response){
                if (response.data === "Registrado") {
                    $scope.limpiar();
                    
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: check'></span> Servicio registrado con éxito", 
                        status: 'success', 
                        pos: 'bottom-right', 
                        timeout: 5000 
                    });
                }else if (response.data === "Servicio ya existe") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> El servicio ya está ofertado", 
                        status: 'warning', 
                        pos: 'bottom-right', 
                        timeout: 5000 
                    });
                }else if (response.data === "Faltan datos") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> Ingrese todos los datos", 
                        status: 'warning', 
                        pos: 'bottom-right', 
                        timeout: 5000
                    });
                }
            },
            function(response){
                $scope.user = response2.data || 'Request failed';
            }
        );
    }
    
    $scope.limpiar = function(){
        $scope.nombre = "";
        $scope.descripcion = "";
        $scope.precio = "";
    }
}])
