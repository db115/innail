var app = angular.module('appControl');

app.controller('conRegistroEmpleado', ['$scope', '$http', function($scope, $http){
    $scope.user = {nombre: '', email: '', telefono: '', direccion: '', password: ''};
    
    $scope.registrar = function(){
        $http({
            method: 'POST',
            url: '/administrador/empleado/contratar/',
            data: $.param({nombre: $scope.nombre, email: $scope.email, telefono: $scope.telefono, direccion: $scope.direccion, password: $scope.password})
        }).then(
            function(response){
                if (response.data === "Registrado") {
                    $scope.limpiar();
                    
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: check'></span> Empleado registrado con éxito", 
                        status: 'success', 
                        pos: 'bottom-right', 
                        timeout: 5000 
                    });
                }else if (response.data === "Empleado ya existe") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> El empleado ya está registrado", 
                        status: 'warning', 
                        pos: 'bottom-right', 
                        timeout: 5000 
                    });
                }else if (response.data === "Faltan datos") {
                    UIkit.notification({ 
                        message: "<span uk-icon='icon: warning'></span> Ingrese todos los datos", 
                        status: 'warning', 
                        pos: 'bottom-right', 
                        timeout: 5000
                    });
                }
            },
            function(response){
                $scope.user = response2.data || 'Request failed';
            }
        );
    }
    
    $scope.limpiar = function(){
        $scope.nombre = "";
        $scope.email = "";
        $scope.telefono = "";
        $scope.direccion = "";
        $scope.password = "";
    }
}])
