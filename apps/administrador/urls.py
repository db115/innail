from django.conf.urls import url
from apps.administrador import views

urlpatterns = [
    url(r'^perfil/$', views.perfil, name="perfil"),
    url(r'^clientes/$', views.clientes, name="clientes"),
    url(r'^lista_clientes/$', views.listar_clientes, name="lista_clientes"),
    url(r'^empleados/registrar/$', views.registrar_empleado, name="registrar_empleados"),
    url(r'^empleado/contratar/$', views.contratar_empleado, name="contratar_empleado"),
    url(r'^empleados/$', views.empleados, name="empleados"),
    url(r'^lista_empleados/$', views.listar_empleados, name="lista_empleados"),
    url(r'^servicios/registrar/$', views.registrar_servicios, name="registrar_servicios"),
    url(r'^servicios/ofertar/$', views.ofrecer_servicio, name="ofertar_servicios"),
    url(r'^servicios/$', views.servicios, name="servicios"),
]
