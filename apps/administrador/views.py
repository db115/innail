import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from apps.usuario.models import tipoRol
from apps.usuario.models import Usuario
from apps.principal.models import Servicio

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

@login_required(login_url='/ingresar/',redirect_field_name=None)
def perfil(request):
    pag="control/perfil.html"

    if request.user.usuario.rol_id==1:
        return render(request, pag)
    elif request.user.usuario.rol_id==2:
        return HttpResponseRedirect('/empleado/perfil/')
    elif request.user.usuario.rol_id==3:
        return HttpResponseRedirect('/cliente/perfil/')

    return HttpResponseRedirect('/')

@login_required(login_url='/ingresar/',redirect_field_name=None)
def clientes(request):
    pag="administrador/clientes.html"

    if request.user.usuario.rol_id==1:
        return render(request, pag)
    elif request.user.usuario.rol_id==2:
        return HttpResponseRedirect('/')
    elif request.user.usuario.rol_id==3:
        return HttpResponseRedirect('/cliente/perfil/')

    return HttpResponseRedirect('/')

@login_required(login_url='/ingresar/',redirect_field_name=None)
def listar_clientes(request):
    clientes = Usuario.objects.values('nombre','usuario__username','telefono').filter(rol_id=3)
    lista_clientes = list(clientes)

    return HttpResponse(json.dumps(lista_clientes), content_type="application/json")

@login_required(login_url='/ingresar/',redirect_field_name=None)
def registrar_empleado(request):
    pag="administrador/registrar_empleado.html"

    if request.user.usuario.rol_id==1:
        return render(request, pag)
    elif request.user.usuario.rol_id==2:
        return HttpResponseRedirect('/')
    elif request.user.usuario.rol_id==3:
        return HttpResponseRedirect('/cliente/perfil/')

    return HttpResponseRedirect('/empleados/registrar/')

@login_required(login_url='/ingresar/',redirect_field_name=None)
def contratar_empleado(request):
    if request.method=='POST':
        if 'nombre' in request.POST or 'email' in request.POST or 'telefono' in request.POST or 'direccion' in request.POST or 'password' in request.POST:
            nombre=request.POST['nombre']
            email=request.POST['email']
            telefono=request.POST['telefono']
            direccion=request.POST['direccion']
            password=request.POST['password']
            
            if len(nombre)>0 and len(email)>0 and len(telefono)>0 and len(direccion)>0 and len(password)>0:
                if User.objects.filter(username=email, is_active=True).exists():
                    return HttpResponse('Empleado ya existe')
                else:
                    user=User.objects.create_user(email, email, password)
                    user.save()

                    tRol=tipoRol.objects.get(id=2)

                    usermodelo=Usuario(usuario=user, nombre=nombre, telefono=telefono, rol=tRol, direccion=direccion)
                    usermodelo.save()
            
                    return HttpResponse('Registrado')
            else:
                return HttpResponse('Faltan datos')
        else:
            return HttpResponse('Faltan campos')
    
    return HttpResponseRedirect("/empleados/registrar/")

@login_required(login_url='/ingresar/',redirect_field_name=None)
def empleados(request):
    pag="administrador/empleados.html"

    if request.user.usuario.rol_id==1:
        return render(request, pag)
    elif request.user.usuario.rol_id==2:
        return HttpResponseRedirect('/')
    elif request.user.usuario.rol_id==3:
        return HttpResponseRedirect('/cliente/perfil/')

    return HttpResponseRedirect('/')

@login_required(login_url='/ingresar/',redirect_field_name=None)
def listar_empleados(request):
    empleados = Usuario.objects.values('nombre','usuario__username','telefono', 'direccion').filter(rol_id=2)
    lista_empleados = list(empleados)

    return HttpResponse(json.dumps(lista_empleados), content_type="application/json")

@login_required(login_url="/ingresar/",redirect_field_name=None)
def registrar_servicios(request):
    pag="administrador/registrar_servicio.html"

    if request.user.usuario.rol_id==1:
        return render(request, pag)
    elif request.user.usuario.rol_id==3 or request.user.usuario.rol_id==2:
        return HttpResponseRedirect('/catalogo')

    return HttpResponseRedirect('/')

@login_required(login_url='/ingresar/',redirect_field_name=None)
def ofrecer_servicio(request):
    if request.method=='POST':
        if 'nombre' in request.POST or 'descripcion' in request.POST or 'precio' in request.POST:
            nombre=request.POST['nombre']
            descripcion=request.POST['descripcion']
            precio=request.POST['precio']
            
            if len(nombre)>0 and len(descripcion)>0 and len(precio)>0:
                if Servicio.objects.filter(nombre=nombre).exists():
                    return HttpResponse('Servicio ya existe')
                else:
                    servicio=Servicio(nombre=nombre, descripcion=descripcion, precio=precio)
                    servicio.save()
            
                    return HttpResponse('Registrado')
            else:
                return HttpResponse('Faltan datos')
        else:
            return HttpResponse('Faltan campos')
    
    return HttpResponseRedirect("/administrador/servicios/registrar/")

@login_required(login_url='/ingresar/',redirect_field_name=None)
def servicios(request):
    pag="administrador/servicios.html"

    if request.user.usuario.rol_id==1:
        return render(request, pag)
    elif request.user.usuario.rol_id==3 or request.user.usuario.rol_id==2:
        return HttpResponseRedirect('/catalogo')

    return HttpResponseRedirect('/')
