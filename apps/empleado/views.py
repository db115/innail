from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib.auth.decorators import login_required

@login_required(login_url='/ingresar/',redirect_field_name=None)
def perfil(request):
    pag="control/perfil.html"

    if request.user.usuario.rol_id==1:
        return HttpResponseRedirect('/administrador/perfil/')
    elif request.user.usuario.rol_id==2:
        return render(request, pag)
    elif request.user.usuario.rol_id==3:
        return HttpResponseRedirect('/cliente/perfil/')
