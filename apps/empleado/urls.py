from django.conf.urls import url
from apps.empleado import views

urlpatterns = [
    url(r'^perfil/$', views.perfil, name="perfil"),
]
