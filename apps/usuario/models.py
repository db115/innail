from django.db import models
from django.contrib.auth.models import User

class tipoRol(models.Model):
    tipo=models.IntegerField()
    rol=models.CharField(max_length=50, null=True)
    
    def __str__(self):
        return(self.rol)

class Usuario(models.Model):
    usuario=models.OneToOneField(User, on_delete=models.CASCADE, null=False)

    nombre=models.CharField(max_length=255, null=False)
    telefono=models.IntegerField(default=0, null=False)
    rol=models.ForeignKey(tipoRol, default=3)
    direccion=models.CharField(max_length=255, null=True)
    
    def __str__(self):
        return(self.nombre)
