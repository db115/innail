from django.conf.urls import url
from apps.cliente import views

urlpatterns = [
    url(r'^registro/$', views.registrarCliente, name='registrar'),
    
    url(r'^perfil/$', views.perfil, name="perfil"),
    url(r'^perfil/actualizar/$', views.actualizar_perfil, name="actualizar_perfil"),
]
