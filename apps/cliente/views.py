from django.shortcuts import render
from django.core import  serializers
from django.http import HttpResponse, HttpResponseBadRequest, HttpRequest, HttpResponseRedirect

from apps.usuario.models import Usuario

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

def registrar(request):
    pag='cliente/registrar.html'
    
    if not request.user.is_authenticated():
        return render(request,pag)
        
    return HttpResponseRedirect('/cliente/perfil/')

def registrarCliente(request):
    if request.method=='POST':
        if 'nombre' in request.POST or 'email' in request.POST or 'telefono' in request.POST or 'password' in request.POST:
            nombre=request.POST['nombre']
            email=request.POST['email']
            telefono=request.POST['telefono']
            password=request.POST['password']
            
            if len(nombre)>0 and len(email)>0 and len(telefono)>0 and len(password)>0:
                if User.objects.filter(username=email).exists():
                    return HttpResponse('Cliente ya existe')
                else:
                    user=User.objects.create_user(email, email, password)
                    user.save()
            
                    usermodelo=Usuario(usuario=user, nombre=nombre, telefono=telefono)
                    usermodelo.save()
            
                    userfinal=authenticate(username=email, password=password)
                    login(request, userfinal)
            
                    return HttpResponse('Registrado')
            else:
                return HttpResponse('Faltan datos')
        else:
            return HttpResponse('Faltan campos')
    
    return HttpResponseRedirect("/registrar/")

@login_required(login_url='/ingresar/',redirect_field_name=None)
def perfil(request):
    pag="cliente/perfil.html"
    
    if request.user.usuario.rol_id==3:
        return render(request, pag)
    elif request.user.usuario.rol_id==2 or request.user.usuario.rol_id==1:
        return HttpResponseRedirect('/control/tablero/')
    
    return HttpResponseRedirect("/")
    
@login_required(login_url='/ingresar/',redirect_field_name=None)
def actualizar_perfil(request):
    if request.method=='POST':
        if 'nombre' in request.POST or 'telefono' in request.POST or 'password' in request.POST:
            nombre=request.POST['nombre']
            email=request.session.email
            telefono=request.POST['telefono']
            
            if len(nombre)>0 and len(telefono)>0:
                user=Usuario.objects.get(usuario__username=email)
                user=Usuario(nombre=nombre, telefono=telefono)
                user.save()
                
                return HttpResponse('Actualizado')
            else:
                return HttpResponse('Faltan datos')
        else:
            return HttpResponse('Faltan campos')

    return HttpResponseRedirect("/cliente/perfil/")
