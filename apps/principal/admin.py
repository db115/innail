from django.contrib import admin

from apps.principal.models import Servicio
from apps.usuario.models import Usuario, tipoRol

admin.site.register(Usuario)
admin.site.register(tipoRol)
admin.site.register(Servicio)
