from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core import  serializers

from apps.usuario.models import Usuario
from apps.principal.models import Servicio

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

""" <----------------> Llamado a plantillas </----------------> """

def ingresar(request):
    pag="principal/ingresar.html"
    
    if not request.user.is_authenticated():
        return render(request,pag)
        
    return HttpResponseRedirect('/cliente/perfil/')

def inicio(request):
    pag="principal/inicio.html"
    
    return render(request,pag)

@login_required(login_url='/ingresar/',redirect_field_name=None)
def control(request):
    pag="control/tablero.html"
    
    if request.user.usuario.rol_id==1 or request.user.usuario.rol_id==2:
        return render(request, pag)
    elif request.user.usuario.rol_id==3:
        return HttpResponseRedirect('/cliente/perfil/')
    
    return HttpResponseRedirect('/')

def catalogo(request):
    pag="cliente/catalogo.html"
    
    return render(request,pag)

""" <----------------> Ingreso y salida del sistema </----------------> """

def iniciarSesion(request):
    if request.method=='POST':
        email=request.POST['email']
        password=request.POST['password']

        user=authenticate(username=email, password=password)

        if user is not None:
            login(request, user)

            us = Usuario.objects.get(usuario__username=request.POST['email'])
            return HttpResponse(str(us.rol.tipo))
        else:
            return HttpResponse('Datos incorrectos')

    return HttpResponseRedirect("/ingresar/")

def salir(request):
    logout(request)

    return HttpResponseRedirect("/")

""" <----------------> Ingreso y salida del sistema </----------------> """

def listarServicios(request):
    #SELECT "principal_servicio"."id", "principal_servicio"."nombre", "principal_servicio"."descripcion", "principal_servicio"."ofertado", "principal_servicio"."precio" FROM "principal_servicio" WHERE "principal_servicio"."ofertado" = True
    servicios=Servicio.objects.filter(ofertado=True)
    data=serializers.serialize('json',servicios)

    return HttpResponse(data, content_type="application/json")
