from django.conf.urls import url
from apps.principal import views
from apps.cliente.views import registrar

urlpatterns = [
    url(r'^$', views.inicio, name='inicio'),
    url(r'^ingresar/$', views.ingresar, name='ingresar'),
    url(r'^registrar/$', registrar, name='registrarCliente'),
    
    url(r'^control/tablero/$', views.control, name='tablero'),
    
    url(r'^ingreso/$', views.iniciarSesion, name='ingreso'),
    url(r'^salir/$', views.salir, name="salir"),
    
    url(r'^catalogo/$', views.catalogo, name="catalogo"),
    url(r'^servicios/$', views.listarServicios, name="servicios"),
]
