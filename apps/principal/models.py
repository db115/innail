from django.db import models

class Servicio(models.Model):
    nombre=models.CharField(max_length=255)
    descripcion=models.TextField()
    ofertado=models.BooleanField(default=True)
    precio=models.IntegerField(default=0)
    
    def __str__(self):
        return(self.nombre)
